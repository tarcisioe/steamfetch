import json
import sys

from argparse import ArgumentParser
from urllib import request
from pprint import pprint

from command import command, Arg, attach_all

STEAMKEY = '5E3FFE93283083FF2D82358D840C0F1B'

BASE_URL = 'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={}&steamid={}&include_appinfo=1'


@command()
def base(steam_id: Arg()):
    url = BASE_URL.format(STEAMKEY, steam_id)
    response = request.urlopen(url)
    contents = str(response.read(), 'utf8')
    games = json.loads(contents)['response']['games']
    games = sorted((game['playtime_forever'], game['name']) for game in games)

    names = [game for _, game in games]
    for name in names:
        print(name)

    # print(len([game for game in games if game[0] == 0]))


def main(progname, arguments):
    arg_parser = ArgumentParser(prog=progname)

    subparsers = arg_parser.add_subparsers()

    attach_all(subparsers)

    args = arg_parser.parse_args(arguments)

    try:
        args.command(args)
    except AttributeError:
        arg_parser.print_usage()


if __name__ == '__main__':
    main(sys.argv[0], sys.argv[1:])
