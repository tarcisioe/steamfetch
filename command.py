import sys
from os.path import basename

_commands = []


class Arg:
    def __init__(self, *args, **kwargs):
        self.args = args
        self.kwargs = kwargs


class SubCommand:
    def __init__(self, names, arg_format, function):
        self.names = names
        self.arg_format = arg_format
        self.function = function

    def attach(self, subparsers):
        name, *names = self.names
        parser = subparsers.add_parser(name, aliases=names)

        for name, arg in self.arg_format:
            parser.add_argument(name, *(arg.args), **(arg.kwargs))

        def call(args):
            arg_dict = {name: getattr(args, name)
                        for name, _ in self.arg_format}
            self.function(**arg_dict)

        parser.set_defaults(command=call)


def command(names=[], wrapper=None):
    def inner(f):
        function = wrapper(f) if wrapper else f

        names_list = [f.__name__] if not names else names

        arg_format = f.__annotations__.items()

        _commands.append(SubCommand(names_list, arg_format, function))

        return f
    return inner


def get_command(name):
    return _commands[name]


def command_names():
    return _commands.keys()


def attach_all(subparsers):
    for command in _commands:
        command.attach(subparsers)


def fail(message, name=basename(sys.argv[0]), retval=-1):
    print('{}: {}'.format(name, message), file=sys.stderr)
    sys.exit(retval)
